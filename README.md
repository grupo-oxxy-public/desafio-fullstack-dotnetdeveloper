Oportunidade de trabalho no Grupo Oxxy
===========================================

Venha trabalhar na área de tecnologia do Grupo Oxxy. Nosso clima é de extrema colaboração. 

Descrição da Vaga
-------------------------------------------------------
Requisitos técnicos
Back-End:
- Microsoft .Net C#
- Asp.Net Core 2.0 (Web Api - RESTful services)
- Entity Framework Core, Dapper e LINQ
- IDE Visual Studio 2015 ou Superior
- Conhecimento em SOLID, Clean Code, Desing Patterns, DDD, TDD e DI são diferenciais

Front-End:
- HTML5/CSS e Bootstrap 3.x+
- Javascript (ES6 ou superior), TypeScript e Angular(2+ e superior)
- Angular-CLI, pacotes NPM e WebPack
- IDE Visual Studio Code
- Conhecimento em PWA (Ionic, Service Works) e UX são diferenciais

Banco de Dados:
- SQL Server
- IDE SQL Server Management Studio
- Conhecimento NoSQL e Google Firebase são diferenciais

Controle de Versão:
- Git

Responsabilidades
- Perfil de visão de dono, busca a solução dos problemas, mesmo que dependa de outras pessoas
- Trabalho em equipe 
- Proatividade
- Levantamento de Requisitos
- Não se detém por pequenos problemas, busca contorná-los e encontrar as soluções
- Desejo de apreender cada vez mais e compartilhar conhecimentos

Se você tem interesse em fazer parte de uma equipe multidisciplinar, siga os seguintes passos:
 - Crie um repositório no github ou bitbucket para armazenar a resolução do exercício.
 - Envie um email para trabalheconoscoti@grupooxxy.com.br com seu repositório para nós analisarmos a resolução do exercício.

Desafio: Cadastro de Veículos
===========================================

Crie um site SPA que administre um cadastro de veículos em uma base de dados.

O site consiste basicamente em três telas (o layout da tela fica livre para você criar):

 * **Listar os veículos cadastrados** 
   ![Screenshots](lista-01.png)
    
 * **Formulário para cadastro de veículos**
   (O usuário poderá subir um número ilimitado de fotos)
   ![Screenshots](cadastrar.png)
    
 * **Detalhes do veículo** 
   ![Screenshots](visualizar.png)
      
As tecnologias que nós recomendamos a utilizar são as mesmas informadas em nosso requisito técnico da vaga.

Lembrando que será avaliado a estrutura do código criado, padrões utilizados, testes unitários e qualidade. Não precisa se precoupar muito com o layout. Qualquer dúvida sobre o exercício envie um email para trabalheconoscoti@grupooxxy.com.br que responderemos para você!

Boa sorte! =)
